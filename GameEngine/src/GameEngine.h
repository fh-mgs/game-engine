#pragma once

// For use by GameEngine Applications

#include "GameEngine/Application.h"
#include "GameEngine/Log.h"
#include "GameEngine/Layer.h"

// ----------------Entry Point----------------
#include "GameEngine/EntryPoint.h"
// -------------------------------------------