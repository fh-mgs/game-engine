#pragma once

#include "Window.h"

#include <string>
#include <GLFW/glfw3.h>

namespace GameEngine {

	class WindowsWindow : public Window
	{
	public:
		WindowsWindow();
		virtual ~WindowsWindow() = default;
		void Create(int width, int height, std::string title) override;
		int GetWidth() const override;
		int GetHeight() const override;
		std::string GetTitle() const override;

		virtual void* GetNativeWindow() const override;
		// void Resize(int width, int height);
	private:
		int m_Width = 0;
		int m_Height = 0;
		std::string m_Title;

		GLFWwindow* m_Window = nullptr;
		
		static bool s_Initialized;
	};

}