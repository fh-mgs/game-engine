#include "WindowsWindow.h"

namespace GameEngine {

	bool WindowsWindow::s_Initialized = false;

	WindowsWindow::WindowsWindow() : Window()
	{
		if (!s_Initialized)
		{
			glfwInit(); // TODO check if successful
		}

		s_Initialized = true;
	}

	void WindowsWindow::Create(int width, int height, std::string title)
	{
		m_Width = width;
		m_Height = height;
		m_Title = title;

		m_Window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
		if (!m_Window)
		{
			// TODO: Do something
		}

		glfwMakeContextCurrent(m_Window);
	}

	int WindowsWindow::GetWidth() const
	{
		return m_Width;
	}

	int WindowsWindow::GetHeight() const
	{
		return m_Height;
	}

	std::string WindowsWindow::GetTitle() const
	{
		return m_Title;
	}

	void* WindowsWindow::GetNativeWindow() const
	{
		return m_Window;
	}

}