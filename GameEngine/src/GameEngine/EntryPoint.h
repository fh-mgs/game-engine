#pragma once

#ifdef GE_PLATFORM_WINDOWS

extern GameEngine::Application* GameEngine::CreateApplication();

int main(int argc, char** argv) 
{
	GameEngine::Log::Init();
	GE_CORE_WARN("Initialized Log!");
	int a = 5;
	GE_INFO("Hello! Var={0}", a);

	//GE_CORE_TRACE("Core Trace");
	//GE_CORE_INFO("Core Info");
	//GE_CORE_WARN("Core Warn");
	//GE_CORE_ERROR("Core Error");
	//GE_CORE_CRITICAL("Core Critical");

	//GE_TRACE("Trace");
	//GE_INFO("Info");
	//GE_WARN("Warn");
	//GE_ERROR("Error");
	//GE_CRITICAL("Critical");

	auto app = GameEngine::CreateApplication();
	app->Run();
	delete app;
}

#endif