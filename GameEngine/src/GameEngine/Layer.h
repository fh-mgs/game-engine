#pragma once

#include "Core.h"

#include <string>

namespace GameEngine {

	class GAMEENGINE_API Layer
	{
	public:
		Layer(const std::string& name = "Layer");
		virtual ~Layer() = default;

		virtual void OnAttach() {};
		virtual void OnDetach() {};
		virtual void OnUpdate() {};
		// TODO: use time for render
		virtual void OnRender(double timeSinceUpdate) {};

		inline const std::string& GetName() const { return m_DebugName; }
	protected:
		std::string m_DebugName;
	};

}