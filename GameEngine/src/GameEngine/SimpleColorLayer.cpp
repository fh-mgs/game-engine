#include "SimpleColorLayer.h"

#include <cmath>

#include "Application.h"
#include "WindowsWindow.h"

namespace GameEngine {

	SimpleColorLayer::SimpleColorLayer() : Layer("Simple Color Layer")
	{
		glfwInit();
	}

	SimpleColorLayer::~SimpleColorLayer()
	{
		delete m_Window;
	}

	void SimpleColorLayer::OnAttach()
	{
		m_Window = new WindowsWindow();
		m_Window->Create(640, 480, "GUI");
	}

	void SimpleColorLayer::OnDetach()
	{
	}

	void SimpleColorLayer::OnUpdate()
	{
		m_Red += m_RedStep;
		m_Green += m_GreenStep;
		m_Blue += m_BlueStep;

		if (m_Red < 0.0f && m_RedStep < 0)
		{
			m_Red = (int)m_Red - m_Red;
			m_RedStep *= (-1.0f);
		}
		else if (m_Red > 1.0f && m_RedStep > 0)
		{
			m_Red = 1 - (m_Red - (int)m_Red);
			m_RedStep *= (-1.0f);
		}

		if (m_Green < 0.0f && m_GreenStep < 0)
		{
			m_Green = (int)m_Green - m_Green;
			m_GreenStep *= (-1.0f);
		}
		else if (m_Green > 1.0f && m_GreenStep > 0)
		{
			m_Green = 1 - (m_Green - (int)m_Green);
			m_GreenStep *= (-1.0f);
		}

		if (m_Blue < 0.0f && m_BlueStep < 0)
		{
			m_Blue = (int)m_Blue - m_Blue;
			m_BlueStep *= (-1.0f);
		}
		else if (m_Blue >= 1.0f && m_BlueStep > 0)
		{
			m_Blue = 1 - (m_Blue - (int)m_Blue);
			m_BlueStep *= (-1.0f);
		}

	}

	void SimpleColorLayer::OnRender(double timeSinceUpdate)
	{
		/* Render here */
		glClear(GL_COLOR_BUFFER_BIT);

		auto red = m_Red + m_RedStep * timeSinceUpdate;
		auto green = m_Green + m_GreenStep * timeSinceUpdate;
		auto blue = m_Blue + m_BlueStep * timeSinceUpdate;

		glClearColor(red, green, blue, 0.0f);

		/* Swap front and back buffers */
		glfwSwapBuffers(static_cast<GLFWwindow*>(m_Window->GetNativeWindow()));

		/* Poll for and process events */
		glfwPollEvents();

	}

}