#include "Application.h"
#include "WindowsWindow.h"
#include "SimpleColorLayer.h"

namespace GameEngine {

	//Application* Application::s_Instance = nullptr;

	Application::Application()
	{
		//if (!s_Instance) {
		//	s_Instance = this;
		//	m_Window = new WindowsWindow();
		//	m_Window->Create(640, 480, "GUI");
		//}

		m_Layers.push_back(new SimpleColorLayer());

		for (auto layer : m_Layers)
		{
			layer->OnAttach();
		}
	}

	Application::~Application()
	{
		//delete m_Window;

		for (auto layer : m_Layers)
		{
			delete layer;
		}
	}

	void Application::ProcessInput()
	{
		// TODO
	}

	void Application::Update()
	{
		for (const auto& layer : m_Layers)
		{
			layer->OnUpdate();
		}
	}

	void Application::Render(double timeSinceUpdate)
	{
		for (const auto& layer : m_Layers)
		{
			// TODO: use time
			layer->OnRender(timeSinceUpdate);
		}
	}

	void Application::Run()
	{
		double previous = glfwGetTime() * 1000;
		double lag = 0.0;
		while (true)
		{
			double current = glfwGetTime() * 1000;
			double elapsed = current - previous;
			previous = current;
			lag += elapsed;

			ProcessInput();

			while (lag >= MS_PER_UPDATE)
			{
				Update();
				lag -= MS_PER_UPDATE;
			}

			Render(lag / MS_PER_UPDATE);
		}
	}
}