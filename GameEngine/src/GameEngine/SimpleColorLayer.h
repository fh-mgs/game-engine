#pragma once

#include "Layer.h"
#include "Window.h"

namespace GameEngine {

	class SimpleColorLayer : public Layer
	{
	public:
		SimpleColorLayer();
		~SimpleColorLayer();

		virtual void OnAttach() override;
		virtual void OnDetach() override;
		virtual void OnUpdate() override;
		virtual void OnRender(double timeSinceUpdate) override;

	private:
		Window* m_Window;

		float m_Red = 0.0f;
		float m_Green = 0.0f;
		float m_Blue = 0.0f;

		float m_RedStep = 0.05f;
		float m_GreenStep = 0.1f;
		float m_BlueStep = 0.15f;
	};

}