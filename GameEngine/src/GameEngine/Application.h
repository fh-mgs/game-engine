#pragma once

#include "Core.h"
#include "Layer.h"
#include "Window.h"

#include <vector>

namespace GameEngine {

	class GAMEENGINE_API Application
	{
	public:
		Application();
		virtual ~Application();

		void Run();

		//Window& GetWindow() { return *m_Window; }

		//static Application& Get() { return *s_Instance; }
		
	private:
		void ProcessInput();
		void Update();
		void Render(double timeSinceUpdate);

		const double MS_PER_UPDATE = 500.0; // 60 fps
		std::vector<GameEngine::Layer*> m_Layers;

		//Window* m_Window = nullptr;

		//static Application* s_Instance;
	};

	// To be defined in CLIENT
	Application* CreateApplication();

}