#pragma once

#include <string>

namespace GameEngine {

	class Window
	{
	public:
		virtual ~Window() = default;

		virtual void Create(int width, int height, std::string title) = 0;
		virtual int GetWidth() const = 0;
		virtual int GetHeight() const = 0;
		virtual std::string GetTitle() const = 0;

		virtual void* GetNativeWindow() const = 0;
		// void Resize(int width, int height);
	};

}