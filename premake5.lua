workspace "GameEngine" -- solution
	architecture "x64"
	
	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

-- Include directories relative to root folder (solution directory)
IncludeDir = {}
IncludeDir["GLFW"] = "GameEngine/vendor/GLFW/include"

include "GameEngine/vendor/GLFW"

project "GameEngine"
	location "GameEngine" -- file path
	kind "SharedLib" -- dynamic library
	language "C++"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}") -- directory for the compiled binary target
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}") -- directory for object and intermediate files

	files -- add files to project
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp",
	}

	includedirs -- include file search paths for the compiler
	{
		"%{prj.name}/src",
		"%{prj.name}/vendor/spdlog/include",
		"%{IncludeDir.GLFW}"
	}

	links
	{
		"GLFW",
		"opengl32.lib"
	}

	filter "system:windows" -- only apply if windows
		cppdialect "C++17"
		staticruntime "On"
		systemversion "latest"

		defines -- define preprocessor or compiler symbols
		{
			"GE_PLATFORM_WINDOWS",
			"GE_BUILD_DLL"
		}

		postbuildcommands -- put dll in Sandbox folder after build is finished
		{
			("{COPY} %{cfg.buildtarget.relpath} ../bin/" .. outputdir .. "/Sandbox")
		}

	filter "configurations:Debug" -- only apply if Debug
		defines "GE_DEBUG"
		symbols "On" -- Turn on debug symbol table generation

	filter "configurations:Release" -- only apply if Release
		defines "GE_RELEASE"
		optimize "On" -- Perform a balanced set of optimizations

	filter "configurations:Dist" -- only apply if Dist
		defines "GE_DIST"
		optimize "On" -- Perform a balanced set of optimizations

project "Sandbox"
	location "Sandbox" -- file path
	kind "ConsoleApp"
	language "C++"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}") -- directory for the compiled binary target
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}") -- directory for object and intermediate files

	files -- add files to project
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp",
	}

	includedirs -- include file search paths for the compiler
	{
		"GameEngine/vendor/spdlog/include",
		"GameEngine/src"
	}

	links -- list of libraries and projects to link against
	{
		"GameEngine"
	}

	filter "system:windows" -- only apply if windows
		cppdialect "C++17"
		staticruntime "On"
		systemversion "latest"

		defines -- define preprocessor or compiler symbols
		{
			"GE_PLATFORM_WINDOWS",
		}

	filter "configurations:Debug" -- only apply if Debug
		defines "GE_DEBUG"
		symbols "On" -- Turn on debug symbol table generation

	filter "configurations:Release" -- only apply if Release
		defines "GE_RELEASE"
		optimize "On" -- Perform a balanced set of optimizations

	filter "configurations:Dist" -- only apply if Dist
		defines "GE_DIST"
		optimize "On" -- Perform a balanced set of optimizations